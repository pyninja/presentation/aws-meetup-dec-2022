PK     =m�UT�W�h  h     microkit/datamodel.pyimport abc
import dataclasses


@dataclasses.dataclass
class BaseDataModel(abc.ABC):
    PK: str  # attr#entity_type
    SK: str  # id

    @classmethod
    def from_dict(self, d):
        """create self from a dictionary"""
        return self(**d)

    def to_dict(self):
        """Convert itself to a dictionary"""
        return dataclasses.asdict(self)

PK     =m�U               microkit/__init__.pyPK     =m�U�Vɍ�  �     microkit/utils.pyimport base64
import decimal
import json
import os
import re
import typing as t
import unicodedata
from datetime import datetime, timedelta, timezone

REGEX_COLLECTION = [
    {"pattern": re.compile(r" +"), "replace_with": "-"},  # multi_space_replacer
    {"pattern": re.compile(r"[^0-9a-zA-Z-]"), "replace_with": ""},  # special_character_replacer
]

def remove_accents(input_str: str) -> str:
    nfkd_form = unicodedata.normalize('NFKD', input_str)
    return u"".join([c for c in nfkd_form if not unicodedata.combining(c)])


def convert_to_internal_convention(text: str) -> str:
    text = remove_accents(text)
    for regex in REGEX_COLLECTION:
        text = regex["pattern"].sub(regex["replace_with"], text)
    return remove_accents(text.lower())


def collect_unix_utc_seconds() -> int:
    now = datetime.now(tz=timezone.utc).timestamp()
    return int(now)

def collect_utc_now() -> str:
    return datetime.now(tz=timezone.utc).strftime("%Y-%m-%dT%H:%M:%S")


def collect_cet_now() -> str:
    """Convert current machine time into CET time"""
    off_set = 2
    utc_now = datetime.now(tz=timezone.utc)
    result = utc_now + timedelta(hours=off_set)
    return result.strftime("%Y-%m-%dT%H:%M:%S")


def collect_utc_date() -> str:
    return datetime.now(tz=timezone.utc).date().strftime("%Y-%m-%d")


def create_local_path(filename: str) -> str:
    if os.environ["ENVIRON"] == "local":
        return f"temp/{filename}"
    return f"/tmp/{filename}"

class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            try:
                return int(o)
            except BaseException:
                return str(o)
        return super(DecimalEncoder, self).default(o)


def all_field_provided(payload: t.Dict, required_list: t.List) -> t.Tuple[bool, str]:
    missing_list = []
    for item in required_list:
        if item not in payload:
            missing_list.append(item)
    if len(missing_list) > 0:
        return False, "Following Fields are missing {}".format(", ".join(missing_list))
    return True, "OK"


def increment_pointer(current_pointer: str) -> str:
    """Increase version of the pointer"""
    new_number = int(re.search("\\d+", current_pointer).group()) + 1
    new_version = re.sub("\\d+", str(new_number), current_pointer)
    return new_version


def add_s3_prefix(path: str) -> str:
    """Add prefix to a s3 code"""
    return f"s3://{path}"


def return_by_status_code(response: t.Dict, error=None) -> t.Dict:
    """Return any database operation result based on status code"""
    if response["status"] == 200:
        response["error"] = None
        return response
    if error:
        response["error"] = error
        return response
    response["error"] = "ClientError: Could not get/post the data into database"
    return response


def bytes_to_base64_str(byte_data: bytes) -> str:
    """Convert bytes to base 64"""
    b64_encoded_byte = base64.b64encode(byte_data)
    b64_encoded_str = b64_encoded_byte.decode("utf-8")
    return b64_encoded_str


def create_metacomposit(attr_list: t.List[str]) -> str:
    """Combining the component from a list into a string separeted by #"""
    attr_list_flatten = []
    for attr in attr_list:
        if isinstance(attr, list):
            attr_list_flatten.extend(attr)
        else:
            attr_list_flatten.append(attr)
    attr_list_flatten = [convert_to_internal_convention(x) for x in attr_list_flatten]
    metacomposit = "#".join(attr_list_flatten)
    return metacomposit


def encode_b64_str(source: str):
    """String to Base 64 string"""
    base64_bytes = source.encode('utf-8')
    content = base64.b64encode(base64_bytes).decode('utf-8')
    return content


def decode_b64_str(source: str):
    """Base 64 encoded sring to string"""
    base64_bytes = source.encode('utf-8')
    message_bytes = base64.b64decode(base64_bytes)
    content = message_bytes.decode('utf-8')
    return content
PK     =m�Uױ�{  {     microkit/orm.pyimport dataclasses
import os

from boto3.dynamodb.table import TableResource
from botocore.exceptions import ClientError

from .datamodel import BaseDataModel
from .utils import add_s3_prefix


def create_path_from_source(resp: dict, source: str) -> str:
    """By given the response create the s3 path of the dictionary"""
    if resp["ResponseMetadata"]["HTTPStatusCode"] == 200:
        data = resp["Item"]
        if data["bucket"] and data["bucket_key"]:
            path = add_s3_prefix(os.path.join(data["bucket"], data["bucket_key"]))
            return path
        else:
            raise Exception(
                f"{source} location metadata is missing. ",
                f"You probably have not submitted any {source} for the project ",
                f"Or the {source} is not configured properly. ",
                "Please check if you have followed all the steps necessary to submit current request."
            )
    raise Exception("Could Not find metadata in database")


def get_model_artifact_path(resource: TableResource) -> str:
    resp = resource.get_item(Key={"PK": "artf#artifact", "SK": "model"})
    return create_path_from_source(resp, "Model")


def get_job_artifact_path(resource: TableResource, job_type: str) -> str:
    resp = resource.get_item(Key={"PK": "artf#artifact", "SK": f"job#{job_type}"})
    return create_path_from_source(resp, "Job")


def get_latest_dictionary_path(resource: TableResource) -> str:
    resp = resource.get_item(Key={"PK": "dict#dictionary", "SK": "version#v0"})
    return create_path_from_source(resp, "Dictionary")


def get_project_info_by_pid(resource: TableResource, pid: str) -> dict:
    resp = resource.get_item(Key={"PK": "proj#project", "SK": pid})
    if "Item" in resp:
        return resp
    return None


def get_dossier_path(resource: TableResource, pid: str) -> str:
    resp = get_project_info_by_pid(resource=resource, pid=pid)
    return create_path_from_source(resp, "Dossier")


def get_job_output_path(resource: TableResource, pid: str, job_type: str) -> str:
    resp = resource.get_item(Key={"PK": f"proj#{pid}", "SK": f"{job_type}#v0"})
    return create_path_from_source(resp, "Processing Job")


@dataclasses.dataclass
class DynamoOrm:
    """Handler composition for CRUD operation to dynamodb"""
    data: BaseDataModel
    table: TableResource

    def create_response(self, status: int, data=None):
        """Generic response creating method"""
        if data:
            return {"status": status, "data": data}
        return {"status": status, "data": self.data.to_dict()}

    def put_item(self):
        """Put item wrapper over dynamodb with conditional check"""
        try:
            response = self.table.put_item(
                Item=self.data.to_dict(),
                ConditionExpression="attribute_not_exists(PK) AND attribute_not_exists(SK)"
            )
            return self.create_response(response["ResponseMetadata"]["HTTPStatusCode"])
        except ClientError:
            return self.create_response(500)

    def get_item(self):
        """Get item wrapper for dynamo db"""
        response = self.table.get_item(
            Key={"PK": self.data.PK, "SK": self.data.SK}
        )
        if "Item" not in response:
            return self.create_response(500)
        return self.create_response(response["ResponseMetadata"]["HTTPStatusCode"], response["Item"])

    def get_item_begins_with(self, pattern: str):
        """Get item wrapper for dynamo db"""
        response = self.table.query(
            KeyConditionExpression="PK = (:val0) AND begins_with (SK, :val1)",
            ExpressionAttributeValues={':val0': self.data.PK, ":val1": pattern}
        )
        if "Items" not in response:
            return self.create_response(500)
        if len(response["Items"]) == 0:
            return self.create_response(500)
        return self.create_response(response["ResponseMetadata"]["HTTPStatusCode"], response["Items"])

    def get_item_by_index_begins_with(self, index: str, hash_key: dict, range_pattern: dict):
        """Get item wrapper for dynamo db"""
        pk = hash_key["name"]
        sk = range_pattern["name"]
        response = self.table.query(
            IndexName="JobStatusIndex",
            KeyConditionExpression=f"{pk} = (:val0) AND begins_with ({sk}, :val1)",
            ExpressionAttributeValues={':val0': hash_key["value"], ":val1": range_pattern["pattern"]}
        )
        if "Items" not in response:
            return self.create_response(500)
        if len(response["Items"]) == 0:
            return self.create_response(500)
        return self.create_response(response["ResponseMetadata"]["HTTPStatusCode"], response["Items"])

    def update_item(self, *args):
        """Update item wrapper for dynamodb"""
        for arg in args:
            if not hasattr(self.data, arg):
                raise AttributeError(f"Addribute {arg} does not exist")

        expression_deck = ", ".join([f"{item} = :val{i}" for i, item in enumerate(args)])
        exp = f"SET {expression_deck}"
        value_map = {f":val{i}": self.data.to_dict()[arg] for i, arg in enumerate(args)}
        try:
            response = self.table.update_item(
                Key={
                    "PK": self.data.PK,
                    "SK": self.data.SK
                },
                UpdateExpression=exp,
                ExpressionAttributeValues=value_map,
                ConditionExpression="attribute_exists(PK) AND attribute_exists(SK)"

            )
            return self.create_response(response["ResponseMetadata"]["HTTPStatusCode"])
        except ClientError:
            return self.create_response(500)

    def delete_item(self):
        try:
            response = self.table.delete_item(
                Key={
                    "PK": self.data.PK,
                    "SK": self.data.SK
                },
                ConditionExpression="attribute_exists(PK) AND attribute_exists(SK)"
            )
            return self.create_response(response["ResponseMetadata"]["HTTPStatusCode"])
        except ClientError:
            return self.create_response(500)

    def update_all(self):
        """Put item wrapper over dynamodb with conditional check"""
        try:
            response = self.table.put_item(
                Item=self.data.to_dict()
            )
            return self.create_response(response["ResponseMetadata"]["HTTPStatusCode"])
        except ClientError:
            return self.create_response(500)
PK     �n�U|#       microkit/logger.pyimport logging
import sys


class Logger:
    def __init__(self, environ: str, context: str):
        self.environ = environ
        _ = logging.basicConfig(
            format='%(asctime)s [%(levelname)s] %(name)s - %(message)s',
            level=logging.INFO,
            datefmt='%Y-%m-%d %H:%M:%S',
            stream=sys.stdout
        )
        self.logger = logging.getLogger(context)

    def printlog(self, text: str):
        if self.environ == "local":
            self.logger.info(text)
        else:
            print(text)
PK     =m�U�TaE�  �     microkit/exceptions.pyimport json
import typing as t


class ContentNotFoundError(Exception):
    """Exception class to handle database operation error when inserting data or updating data"""

    def __init__(self, message=None):
        self.message = self.create_message(message)
        super().__init__(self.message)

    def create_message(self, custom_msg: t.Optional[str] = None):
        if custom_msg:
            return custom_msg
        return "File not found in the requested location"


class ParameterMissingError(Exception):
    """Exception raised for errors in the input salary.

    Attributes:
        expected, requested
    """

    def __init__(self, expected, requested) -> None:
        """Init for the ParameterMissingError class"""
        self.expected = expected
        self.requested = requested
        self.message = self.create_message()
        super().__init__(self.message)

    def create_message(self) -> str:
        """Create error message from the validation"""
        missing_fields = []
        for item in self.expected:
            if item not in self.requested:
                missing_fields.append(item)
        return "Following field are requered to query the api: {}".format(", ".join(missing_fields))


class ParameterValueError(Exception):
    def __init__(self, expected: list, requested: str):
        self.expected = expected
        self.requested = requested
        self.message = self.create_message()
        super().__init__(self.message)

    def create_message(self):
        return "{} is not a valid query parameter for the request. The Valid parameters are: {}".format(self.requested, ", ".join(self.expected))


class DataBaseOperationError(Exception):
    """Exception class to handle database operation error when inserting data or updating data"""

    def __init__(self, message=None):
        self.message = self.create_message(message)
        super().__init__(self.message)

    def create_message(self, custom_msg: t.Optional[str] = None):
        if custom_msg:
            return custom_msg
        return "Could not peform database operation at the moment."


class JobInitError(Exception):
    """Exception class to handle Stepfunction Job initiation error when starting a job"""

    def __init__(self, message=None):
        self.message = self.create_message(message)
        super().__init__(self.message)

    def create_message(self, custom_msg: t.Optional[str] = None):
        if custom_msg:
            return custom_msg
        return "Could create a new job at the moment."


def query_parameter_ok(expected: t.List, requested: t.Dict) -> bool:
    """Check if the query parameters are properly provided, when request is a dictionary"""
    for item in expected:
        if item not in requested:
            raise ParameterMissingError(expected=expected, requested=requested)
    return True


def parameter_value_ok(expected: t.List, requested: str) -> bool:
    """Test if the parameter values are any valid field, when requeste is a single field"""
    if requested not in expected:
        raise ParameterValueError(expected=expected, requested=requested)
    return True


def create_response_from_exception(exception: Exception, data: t.Union[t.Dict, t.List]) -> t.Dict:
    """Create a Response failed output for the api final request"""
    error_msg = {"errorType": "InternalServerError", "httpStatus": 500, "trace": str(exception)}
    resp = {"statusCode": 500, "data": data, "errorMessage": error_msg}
    return {"statusCode": 500, "body": json.dumps(resp)}


def create_response_from_param_exception(exception: Exception, data: t.Union[t.Dict, t.List]) -> t.Dict:
    """Create a Response failed output for the api final request"""
    error_msg = {"errorType": "ParameterError", "httpStatus": 403, "trace": str(exception)}
    resp = {"statusCode": 403, "data": data, "errorMessage": error_msg}
    return {"statusCode": 403, "body": json.dumps(resp)}


def create_response_from_not_found_exception(exception: Exception, data: t.Union[t.Dict, t.List]) -> t.Dict:
    """Create a Response failed output for the api final request"""
    error_msg = {"errorType": "FileNotFoundError", "httpStatus": 404, "trace": str(exception)}
    resp = {"statusCode": 404, "data": data, "errorMessage": error_msg}
    return {"statusCode": 404, "body": json.dumps(resp)}


PK     �q�UnY"�   �      domainmodel.pyimport dataclasses

from microkit.datamodel import BaseDataModel


@dataclasses.dataclass
class DomainModel(BaseDataModel):
    passPK     �v�U=,�       health_check.py#!/usr/bin/env python
# coding: utf-8

# In[6]:


import json
import logging


from microkit.utils import collect_cet_now

# In[7]:


logging.basicConfig(
    format='%(asctime)s [%(levelname)s] %(name)s - %(message)s',
    level=logging.INFO,
    datefmt='%Y-%m-%d %H:%M:%S',
)
LOGGER = logging.getLogger(__name__)


# In[8]:


def handler(event, context):
    """Handler function for the API gateway"""
    timestamp = collect_cet_now()
    status_code = 200
    resp = {"statusCode": status_code, "data": {"cet_now": timestamp}}
    LOGGER.info("Lambda executed successfully!")
    return {"statusCode": 200, "body": json.dumps(resp)}

PK     =m�UT�W�h  h             ��    microkit/datamodel.pyPK     =m�U                       ���  microkit/__init__.pyPK     =m�U�Vɍ�  �             ���  microkit/utils.pyPK     =m�Uױ�{  {             ���  microkit/orm.pyPK     �n�U|#               ��)+  microkit/logger.pyPK     =m�U�TaE�  �             ��s-  microkit/exceptions.pyPK     �q�UnY"�   �              ���>  domainmodel.pyPK     �v�U=,�               ��P?  health_check.pyPK      �  �A    